<?php
abstract  class Animal{
    public $name;
    public $color;
    public function  describe(){
        return $this->name. ' is '. $this->color;
    }
    abstract public  function makeSound();
}
    class Duck extends Animal{
        public function describe()
        {
            return parent::describe(); // TODO: Change the autogenerated stub
        }
        public  function  makeSound()
        {
            return 'Quack';
        }
    }
class Dog extends Animal{
    public function describe()
    {
        return parent::describe(); // TODO: Change the autogenerated stub
    }
    public  function  makeSound()
    {
        return 'Gau';
    }
}
$animal =new Duck();
$animal ->name="jon";
$animal->color ='Brown';
//echo  $animal->describe();
echo $animal->makeSound();

?>